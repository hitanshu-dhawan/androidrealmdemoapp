package com.hitanshudhawan.realmexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.hitanshudhawan.realmexample.models.UserRealm;

import java.util.List;
import java.util.Random;

import io.realm.Realm;

public class UserActivity extends AppCompatActivity {

    private TextView textView;

    private Integer id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        textView = findViewById(R.id.text_view);

        id = getIntent().getIntExtra("id", -1);

        String text = "";
        for (String friend : fetchFriends()) {
            text += friend + "\n";
        }
        textView.setText(text);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.add_friend:
                addFriend();
                String text = "";
                for (String friend : fetchFriends()) {
                    text += friend + "\n";
                }
                textView.setText(text);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addFriend() {
        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                UserRealm userRealm = realm.where(UserRealm.class)
                        .equalTo("id", id)
                        .findFirst();

                userRealm.getFriends().add(id + " friend " + random());
            }
        });
    }

    private List<String> fetchFriends() {
        Realm realm = Realm.getDefaultInstance();

        UserRealm userRealm = realm.where(UserRealm.class)
                .equalTo("id", id)
                .findFirst();

        return userRealm.getFriends();
    }

    private String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(6);
        char tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }
}
