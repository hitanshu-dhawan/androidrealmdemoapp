package com.hitanshudhawan.realmexample.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hitanshudhawan.realmexample.UserActivity;
import com.hitanshudhawan.realmexample.models.User;
import com.hitanshudhawan.realmexample.models.UserRealm;

import java.util.List;

import io.realm.Realm;

/**
 * Created by hitanshu on 1/4/18.
 */

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {

    private Context context;
    private List<User> users;

    public UsersAdapter(Context context, List<User> users) {
        this.context = context;
        this.users = users;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_1,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textView1.setText(users.get(position).getFirstName() + " " + users.get(position).getLastName());
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView1;

        public ViewHolder(View itemView) {
            super(itemView);
            textView1 = itemView.findViewById(android.R.id.text1);

            textView1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, UserActivity.class);
                    intent.putExtra("id",users.get(getAdapterPosition()).getId());
                    context.startActivity(intent);
                }
            });

            textView1.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    Realm realm = Realm.getDefaultInstance();

                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {

                            realm.where(UserRealm.class)
                                    .equalTo("id",users.get(getAdapterPosition()).getId())
                                    .findAll()
                                    .deleteAllFromRealm();

                            users.remove(getAdapterPosition());
                            notifyDataSetChanged();
                        }
                    });

                    return true;
                }
            });
        }
    }
}
