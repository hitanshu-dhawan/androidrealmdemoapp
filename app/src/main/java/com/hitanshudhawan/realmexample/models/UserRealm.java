package com.hitanshudhawan.realmexample.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hitanshu on 1/4/18.
 */

public class UserRealm extends RealmObject {

    @PrimaryKey
    private Integer id;
    private String firstName;
    private String lastName;
    private Integer age;
    private RealmList<String> friends;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public RealmList<String> getFriends() {
        return friends;
    }

    public void setFriends(RealmList<String> friends) {
        this.friends = friends;
    }
}
