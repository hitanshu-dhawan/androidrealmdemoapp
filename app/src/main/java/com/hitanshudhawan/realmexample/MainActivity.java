package com.hitanshudhawan.realmexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.hitanshudhawan.realmexample.adapters.UsersAdapter;
import com.hitanshudhawan.realmexample.models.User;
import com.hitanshudhawan.realmexample.models.UserRealm;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<User> users;
    private UsersAdapter usersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycler_view);
        users = new ArrayList<>();
        usersAdapter = new UsersAdapter(this, users);
        recyclerView.setAdapter(usersAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        fetchUsers();
        usersAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.add_user:
                addUser();
                fetchUsers();
                usersAdapter.notifyDataSetChanged();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addUser() {
        Realm realm = Realm.getDefaultInstance();

        // runs on UI thread
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Number id = realm.where(UserRealm.class).max("id");

                UserRealm userRealm = new UserRealm();
                userRealm.setId(id == null ? 1 : id.intValue() + 1);
                userRealm.setFirstName("Hitanshu");
                userRealm.setLastName(random());
                userRealm.setAge(21);

                realm.insert(userRealm);
            }
        });

        // runs on background thread
        // The RealmAsyncTask object can cancel any pending transaction
        // if you need to quit the Activity/Fragment before the transaction is completed.
        // Forgetting to cancel a transaction can crash the app if the callback updates the UI!
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                // do something...
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                // optional
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                // optional
            }
        });
    }

    private void fetchUsers() {
        Realm realm = Realm.getDefaultInstance();

        // runs on UI thread
        RealmResults<UserRealm> result = realm.where(UserRealm.class)
                .findAll();

        // runs on background thread
        realm.where(UserRealm.class)
                .findAllAsync();

        users.clear();
        for (UserRealm userRealm : result) {
            User user = new User();
            user.setId(userRealm.getId());
            user.setFirstName(userRealm.getFirstName());
            user.setLastName(userRealm.getLastName());
            user.setAge(userRealm.getAge());
            List<String> friends = new ArrayList<>();
            friends.addAll(userRealm.getFriends());
            user.setFriends(friends);

            users.add(user);
        }
    }

    private String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(6);
        char tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }
}
